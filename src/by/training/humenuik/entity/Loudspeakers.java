package by.training.humenuik.entity;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class Loudspeakers extends Device {
    private int count;

    public Loudspeakers() {
    }

    public void setCount(String count) {
        this.count = Integer.parseInt(count);
    }

    public int getCount() {
        return count;
    }
}
