package by.training.humenuik.entity;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class Monitor extends Device {
    private double diagonal;

    public Monitor() {
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = Double.parseDouble(diagonal);
    }

    public double getDiagonal() {
        return diagonal;
    }
}
