package by.training.humenuik.utils;

import by.training.humenuik.parser.SAXForDevices;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

/**
 * Created by Pavel Humenuik on 21.10.2015.
 */
public class PropertyReader {
    private static final Logger LOG = Logger.getLogger(PropertyReader.class);

    public static ArrayList<String> takeDevicesTags(String tagsType) {
        Properties properties = new Properties();
        ArrayList<String> tags = new ArrayList<>();
        try {
            properties.load(new FileInputStream("properties\\device.properties"));
            tags.addAll(Arrays.asList(properties.getProperty(tagsType).split(";")));
        } catch (IOException e) {
            LOG.error(e);
        }
        return tags;
    }
}
