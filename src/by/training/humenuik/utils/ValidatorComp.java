package by.training.humenuik.utils;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class ValidatorComp {
    private static final Logger LOG = Logger.getLogger(ValidatorComp.class);
    public static final String FILE = "files\\Comp.xml";
    public static final String SCHEMA = "files\\Comp.xsd";

    public static void validate() {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaLocation = new File(SCHEMA);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(FILE);
            validator.validate(source);
            System.out.println(FILE + " is valid.");
        } catch (SAXException e) {
            LOG.error("validation " + FILE + " is not valid because "
                    + e.getMessage());
        } catch (IOException e) {
            LOG.error(FILE + " is not valid because "
                    + e.getMessage());
        }
    }
}