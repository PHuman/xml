package by.training.humenuik.parser;

import by.training.humenuik.entity.Device;

import java.util.HashSet;

public abstract class AbstractParserForDevice {
    static HashSet<Device> devices = new HashSet<>();

    public abstract void parse();

    public  HashSet<Device> getDevices() {
        return devices;
    }
}
