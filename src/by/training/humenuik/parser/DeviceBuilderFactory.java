package by.training.humenuik.parser;

public class DeviceBuilderFactory {
    private enum TypeParser {
        SAX, STAX, DOM
    }
    public static AbstractParserForDevice createDeviceBuilder(String typeParser) {
        TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
        switch (type) {
            case DOM:
                return new DOMForDevices();
            case STAX:
                return new STAXForDevices();
            case SAX:
                return new SAXForDevices();
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}
