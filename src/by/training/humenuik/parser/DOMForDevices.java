package by.training.humenuik.parser;

import by.training.humenuik.entity.*;
import by.training.humenuik.enums.Group;
import by.training.humenuik.enums.Port;
import by.training.humenuik.utils.ValidatorComp;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;

import static by.training.humenuik.utils.PropertyReader.takeDevicesTags;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class DOMForDevices extends AbstractParserForDevice {
    private static final Logger LOG = Logger.getLogger(DOMForDevices.class);
    private Device current;
    private int cursor;

    public void parse() {
        LOG.info("DOM started...");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(ValidatorComp.FILE);
            Element root = doc.getDocumentElement();
            for (int k = 0; k < takeDevicesTags("device").size(); k++) {
                cursor = k;
                NodeList children = root.getElementsByTagName(takeDevicesTags("device").get(k));
                for (int i = 0; i < children.getLength(); i++) {
                    Element child = (Element) children.item(i);
                    Device device = buildDevice(child);
                    devices.add(device);
                }
            }
            LOG.info("DOM finished...");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error(e);
        }
    }

    private  Device buildDevice(Element device) {
        switch (cursor) {
            case 0:
                current = new Mouse();
                ((Mouse) current).setSensor(getElementTextContent(device, takeDevicesTags("method").get(7)));
                break;
            case 1:
                current = new Keyboard();
                ((Keyboard) current).setNum(getElementTextContent(device, takeDevicesTags("method").get(8)));
                break;
            case 2:
                current = new Monitor();
                ((Monitor) current).setDiagonal(getElementTextContent(device, takeDevicesTags("method").get(9)));
                break;
            case 3:
                current = new Loudspeakers();
                ((Loudspeakers) current).setCount(getElementTextContent(device, takeDevicesTags("method").get(10)));
                break;
        }
        setParameters(current, device);
        return current;
    }

    private static void setParameters(Device current, Element device) {
        current.setId(device.getAttribute("id"));
        current.setType(new Type());
        current.setName(getElementTextContent(device, takeDevicesTags("method").get(0)));
        current.setOrigin(getElementTextContent(device, takeDevicesTags("method").get(1)));
        current.setPrice(getElementTextContent(device, takeDevicesTags("method").get(2)));
        current.setCritical(getElementTextContent(device, takeDevicesTags("method").get(3)));
        setTypeParameters(current.getType(), device);
    }

    private static void setTypeParameters(Type type, Element device) {
        NodeList children = device.getElementsByTagName(takeDevicesTags("inner").get(0));
        Element node = (Element) children.item(0);
        if (!node.getAttribute("id").isEmpty()) {
            type.setId(node.getAttribute("id"));
        }
        type.setGroup(Group.valueOf(getElementTextContent(node, takeDevicesTags("method").get(4))));
        type.setPort(Port.valueOf(getElementTextContent(node, takeDevicesTags("method").get(5))));
        type.setPower(getElementTextContent(node, takeDevicesTags("method").get(6)));
    }

    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent();
    }

}
