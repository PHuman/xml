package by.training.humenuik.parser;

import by.training.humenuik.entity.Device;
import by.training.humenuik.utils.ValidatorComp;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class SAXForDevices extends AbstractParserForDevice {
    private static final Logger LOG = Logger.getLogger(SAXForDevices.class);

    public void parse() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            HandlerForDevices hfd = new HandlerForDevices();
            parser.parse(ValidatorComp.FILE, hfd);
            devices = hfd.getDevices();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error(e);
        }
    }

}
