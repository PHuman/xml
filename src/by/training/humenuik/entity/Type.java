package by.training.humenuik.entity;

import by.training.humenuik.enums.Group;
import by.training.humenuik.enums.Port;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class Type {
    private int id;
    private Group group;
    private Port port;
    private double power;

    public Type() {
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id.substring(1));
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setPort(Port port) {
        this.port = port;
    }

    public void setPower(String power) {
        this.power = Double.parseDouble(power);
    }

    public int getId() {
        return id;
    }

    public Group getGroup() {
        return group;
    }

    public Port getPort() {
        return port;
    }

    public double getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id='" + id + '\'' +
                ", group=" + group +
                ", port=" + port +
                ", power=" + power +
                '}';
    }
}
