package by.training.humenuik.entity;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class Keyboard extends Device {
    private boolean num;

    public Keyboard() {
    }

    public void setNum(String num) {
        this.num = Boolean.parseBoolean(num);
    }

    public boolean isNum() {
        return num;
    }
}
