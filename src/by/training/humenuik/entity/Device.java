package by.training.humenuik.entity;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public abstract class Device {
    private int id;
    private String name;
    private String origin;
    private double price;
    private boolean critical;
    private Type type;

    public Device() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOrigin() {
        return origin;
    }

    public double getPrice() {
        return price;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id.substring(1));
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setPrice(String price) {
        this.price = Double.parseDouble(price);
    }

    public void setCritical(String critical) {
        this.critical = Boolean.parseBoolean(critical);
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        if (id != device.id) return false;
        if (Double.compare(device.price, price) != 0) return false;
        if (critical != device.critical) return false;
        if (name != null ? !name.equals(device.name) : device.name != null) return false;
        if (origin != null ? !origin.equals(device.origin) : device.origin != null) return false;
        return !(type != null ? !type.equals(device.type) : device.type != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (origin != null ? origin.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (critical ? 1 : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", critical=" + critical +
                ", type=" + type +
                '}';
    }
}
