package by.training.humenuik.parser;

import by.training.humenuik.entity.*;
import by.training.humenuik.enums.Group;
import by.training.humenuik.enums.Port;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashSet;

import static by.training.humenuik.utils.PropertyReader.*;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class HandlerForDevices extends DefaultHandler {
    private static final Logger LOG = Logger.getLogger(HandlerForDevices.class);
    private HashSet<Device> devices = new HashSet<>();
    private Device current;
    private String tag;
    private boolean flag;

    public HandlerForDevices() {
    }

    @Override
    public void startDocument() throws SAXException {
        LOG.info("SAX started...");
    }

    @Override
    public void endDocument() throws SAXException {
        LOG.info("SAX finished...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        tag = qName;
        if (takeDevicesTags("device").stream().anyMatch(s -> s.equals(tag))) {
            if (takeDevicesTags("device").get(0).equals(tag)) {
                current = new Mouse();
            } else if (takeDevicesTags("device").get(1).equals(tag)) {
                current = new Keyboard();
            } else if (takeDevicesTags("device").get(2).equals(tag)) {
                current = new Monitor();
            } else if (takeDevicesTags("device").get(3).equals(tag)) {
                current = new Loudspeakers();
            }
            current.setId(attributes.getValue(0));
            flag = true;
        }
        if (takeDevicesTags("inner").get(0).equals(tag)) {
            current.setType(new Type());
            if (attributes.getLength() > 0) {
                current.getType().setId(attributes.getValue(0));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (flag) {
            devices.add(current);
            flag = false;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String param = new String(ch, start, length).trim();
        if (!param.isEmpty()) {
            if (takeDevicesTags("method").get(0).equals(tag)) {
                current.setName(param);
            } else if (takeDevicesTags("method").get(1).equals(tag)) {
                current.setOrigin(param);
            } else if (takeDevicesTags("method").get(2).equals(tag)) {
                current.setPrice(param);
            } else if (takeDevicesTags("method").get(3).equals(tag)) {
                current.setCritical(param);
            } else if (takeDevicesTags("method").get(4).equals(tag)) {
                current.getType().setGroup(Group.valueOf(param));
            } else if (takeDevicesTags("method").get(5).equals(tag)) {
                current.getType().setPort(Port.valueOf(param));
            } else if (takeDevicesTags("method").get(6).equals(tag)) {
                current.getType().setPower(param);
            } else if (takeDevicesTags("method").get(7).equals(tag)) {
                ((Mouse) current).setSensor(param);
            } else if (takeDevicesTags("method").get(8).equals(tag)) {
                ((Keyboard) current).setNum(param);
            } else if (takeDevicesTags("method").get(9).equals(tag)) {
                ((Monitor) current).setDiagonal(param);
            } else if (takeDevicesTags("method").get(10).equals(tag)) {
                ((Loudspeakers) current).setCount(param);
            }
        }
    }

    public HashSet<Device> getDevices() {
        return devices;
    }
}
