package by.training.humenuik.enums;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public enum Group {
    INPUT, OUTPUT
}
