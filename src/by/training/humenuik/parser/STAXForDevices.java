package by.training.humenuik.parser;

import by.training.humenuik.entity.*;
import by.training.humenuik.enums.Group;
import by.training.humenuik.enums.Port;
import by.training.humenuik.utils.ValidatorComp;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.HashSet;

import static by.training.humenuik.utils.PropertyReader.takeDevicesTags;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class STAXForDevices extends AbstractParserForDevice {
    private static final Logger LOG = Logger.getLogger(STAXForDevices.class);
    private  Device current;
    private  String tag;

    public void parse() {
        LOG.info("STAX started...");
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try (FileInputStream input = new FileInputStream(new File(ValidatorComp.FILE))) {
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeDevicesTags("device").stream().anyMatch(s -> s.equals(tag))) {
                        current = buildDevice(reader);
                        devices.add(current);
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            LOG.error(e);
        }
        LOG.info("STAX finished...");
    }

    private Device buildDevice(XMLStreamReader reader) throws XMLStreamException {
        if (takeDevicesTags("device").get(0).equals(tag)) {
            current = new Mouse();
        } else if (takeDevicesTags("device").get(1).equals(tag)) {
            current = new Keyboard();
        } else if (takeDevicesTags("device").get(2).equals(tag)) {
            current = new Monitor();
        } else if (takeDevicesTags("device").get(3).equals(tag)) {
            current = new Loudspeakers();
        }
        current.setType(new Type());
        current.setId(reader.getAttributeValue(null, "id"));
        try {
            while (reader.hasNext()) {
                int cursor = reader.next();
                if (cursor == XMLStreamConstants.START_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeDevicesTags("method").get(0).equals(tag)) {
                        current.setName(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(1).equals(tag)) {
                        current.setOrigin(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(2).equals(tag)) {
                        current.setPrice(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(3).equals(tag)) {
                        current.setCritical(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(4).equals(tag)) {
                        current.getType().setGroup(Group.valueOf(getXMLText(reader)));
                    } else if (takeDevicesTags("method").get(5).equals(tag)) {
                        current.getType().setPort(Port.valueOf(getXMLText(reader)));
                    } else if (takeDevicesTags("method").get(6).equals(tag)) {
                        current.getType().setPower(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(7).equals(tag)) {
                        ((Mouse) current).setSensor(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(8).equals(tag)) {
                        ((Keyboard) current).setNum(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(9).equals(tag)) {
                        ((Monitor) current).setDiagonal(getXMLText(reader));
                    } else if (takeDevicesTags("method").get(10).equals(tag)) {
                        ((Loudspeakers) current).setCount(getXMLText(reader));
                    } else if (takeDevicesTags("inner").get(0).equals(tag)) {
                        current.setType(new Type());
                        if (reader.getAttributeValue(null, "id") != null) {
                            current.getType().setId(reader.getAttributeValue(null, "id"));
                        }
                    }
                } else if (cursor == XMLStreamConstants.END_ELEMENT) {
                    tag = reader.getLocalName();
                    if (takeDevicesTags("device").stream().anyMatch(s -> s.equals(tag))) {
                        return current;
                    }
                }
            }
        } catch (XMLStreamException e) {
            LOG.error(e);
        }
        throw new XMLStreamException("Unknown element in tag Device");
    }

    private static String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

}
