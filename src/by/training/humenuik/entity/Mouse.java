package by.training.humenuik.entity;

/**
 * Created by Pavel Humenuik on 20.10.2015.
 */
public class Mouse extends Device {
    private String sensor;

    public Mouse() {
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getSensor() {
        return sensor;
    }
}
