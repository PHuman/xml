package by.training.humenuik.main;

import by.training.humenuik.parser.AbstractParserForDevice;
import by.training.humenuik.parser.DeviceBuilderFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletForDevice extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ServletForDevice.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String log4jLocation = context.getRealPath(config.getInitParameter("log4j"));
        PropertyConfigurator.configure(log4jLocation);
        LOG.info("test");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doParse(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doParse(req, resp);
    }

    private void doParse(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String parserType = req.getParameter("choice");
        LOG.info(parserType);
        AbstractParserForDevice parser = DeviceBuilderFactory.createDeviceBuilder(parserType);
        parser.parse();
        req.setAttribute("result", parser.getDevices());
        req.setAttribute("message", parserType);
        req.getRequestDispatcher("/result.jsp").forward(req, resp);
    }

}
