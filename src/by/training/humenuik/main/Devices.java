package by.training.humenuik.main;

import by.training.humenuik.parser.*;
import by.training.humenuik.utils.ValidatorComp;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Devices {
    static {
        new PropertyConfigurator().configure("web\\WEB-INF\\log4j.properties");
    }

    private static final Logger LOG = Logger.getLogger(Devices.class);

    public static void main(String[] args) {
        ValidatorComp.validate();
        AbstractParserForDevice parser =  DeviceBuilderFactory.createDeviceBuilder("dom");
        parser.parse();
        LOG.info(parser.getDevices().stream().peek(LOG::info).count());

    }
}
