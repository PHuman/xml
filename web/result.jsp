<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${message} парсер</title>
</head>
<body>

<h1>${message} парсер</h1>

<a href="index.jsp">Back to main page</a>
<hr>

<c:forEach var="device" items="${result}">

    <c:choose>
        <c:when test="${'by.training.humenuik.entity.Mouse' eq device['class'].name}" >
            <c:out value="Mouse"/>
        </c:when>
        <c:when test="${'by.training.humenuik.entity.Keyboard' eq device['class'].name}" >
            <c:out value="Keyboard"/>
        </c:when>
        <c:when test="${'by.training.humenuik.entity.Monitor' eq device['class'].name}" >
            <c:out value="Monitor"/>
        </c:when>
        <c:when test="${'by.training.humenuik.entity.Loudspeakers' eq device['class'].name}" >
            <c:out value="Loudspeakers"/>
        </c:when>
    </c:choose>

    <table border="1">
        <tr>
            <th>Поле</th>
            <th>Значение</th>
        </tr>

        <tr>
            <td><b><c:out value="ID"/></b></td>
            <td><c:out value="${device.id}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Company name"/></b></td>
            <td><c:out value="${device.name}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Manufacturer country"/></b></td>
            <td><c:out value="${device.origin}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Price"/></b></td>
            <td><c:out value="${device.price}"/></td>
        </tr>

        <tr>
            <td><b><c:out value="Critical"/></b></td>
            <td><c:out value="${device.critical}"/></td>
        </tr>

        <c:choose>
            <c:when test="${'by.training.humenuik.entity.Mouse' eq device['class'].name}" >
                <td><b><c:out value="Sensor"/></b></td>
                <td><c:out value="${device.sensor}"/></td>
            </c:when>
            <c:when test="${'by.training.humenuik.entity.Keyboard' eq device['class'].name}" >
                <<td><b><c:out value="Num"/></b></td>
                <td><c:out value="${device.num}"/></td>
            </c:when>
            <c:when test="${'by.training.humenuik.entity.Monitor' eq device['class'].name}" >
                <td><b><c:out value="Diagonal"/></b></td>
                <td><c:out value="${device.diagonal}"/></td>
            </c:when>
            <c:when test="${'by.training.humenuik.entity.Loudspeakers' eq device['class'].name}" >
                <td><b><c:out value="Count"/></b></td>
                <td><c:out value="${device.count}"/></td>
            </c:when>
        </c:choose>

        <tr>
            <td><b><c:out value="Type"/></b></td>
            <td></td>
        </tr>

        <tr>
            <c:if test="${device.type.id != 0}">
                <td><b><c:out value="id"/></b></td>
                <td><c:out value="${device.type.id}"/></td>
            </c:if>
        </tr>

        <tr>
            <td><em><b><c:out value="group"/></b></em></td>
            <td><c:out value=" ${device.type.group}"/></td>
        </tr>

        <tr>
            <td><em><b><c:out value="port"/></b></em></td>
            <td><c:out value=" ${device.type.port}"/></td>
        </tr>

        <tr>
            <td><em><b><c:out value="power"/></b></em></td>
            <td><c:out value=" ${device.type.power}"/></td>
        </tr>


            <%--<c:choose>--%>
            <%--<c:when test="${device.amountOnDeposit.currency=='BYR'}">--%>
            <%--<tr>--%>
            <%--<td><b><c:out value="Prolongation ability"/></b></td>--%>
            <%--<td><c:out value="${device.prolongationAbility}"/></td>--%>
            <%--</tr>--%>
            <%--</c:when>--%>
            <%--<c:otherwise>--%>
            <%--<tr>--%>
            <%--<td><b><c:out value="Replenishment"/></b></td>--%>
            <%--<td><c:out value="${device.replenishment}"/></td>--%>
            <%--</tr>--%>

            <%--<tr>--%>
            <%--<td><b><c:out value="Insurance:"/></b></td>--%>
            <%--<td></td>--%>
            <%--</tr>--%>

            <%--<tr>--%>
            <%--<td><em><b><c:out value="currency"/></b></em></td>--%>
            <%--<td><c:out value="${device.insurance.currency}"/></td>--%>
            <%--</tr>--%>

            <%--<tr>--%>
            <%--<td><b><em><c:out value="amount"/></em></b></td>--%>
            <%--<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${device.insurance.amount}"/></td>--%>
            <%--</tr>--%>

            <%--<tr>--%>
            <%--<td><b><em><c:out value="period to pay"/></em></b></td>--%>
            <%--<td><c:out value="${device.insurance.periodToPay}"/></td>--%>
            <%--</tr>--%>
            <%--</c:otherwise>--%>
            <%--</c:choose>--%>
    </table>
    <br/>
</c:forEach>

<a href="index.jsp">Back to main page</a>
</body>
</html>
